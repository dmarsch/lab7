/*******************************************************************************
* File Name: Motor_CLK.h
* Version 2.20
*
*  Description:
*   Provides the function and constant definitions for the clock component.
*
*  Note:
*
********************************************************************************
* Copyright 2008-2012, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_CLOCK_Motor_CLK_H)
#define CY_CLOCK_Motor_CLK_H

#include <cytypes.h>
#include <cyfitter.h>


/***************************************
* Conditional Compilation Parameters
***************************************/

/* Check to see if required defines such as CY_PSOC5LP are available */
/* They are defined starting with cy_boot v3.0 */
#if !defined (CY_PSOC5LP)
    #error Component cy_clock_v2_20 requires cy_boot v3.0 or later
#endif /* (CY_PSOC5LP) */


/***************************************
*        Function Prototypes
***************************************/

void Motor_CLK_Start(void) ;
void Motor_CLK_Stop(void) ;

#if(CY_PSOC3 || CY_PSOC5LP)
void Motor_CLK_StopBlock(void) ;
#endif /* (CY_PSOC3 || CY_PSOC5LP) */

void Motor_CLK_StandbyPower(uint8 state) ;
void Motor_CLK_SetDividerRegister(uint16 clkDivider, uint8 restart) 
                                ;
uint16 Motor_CLK_GetDividerRegister(void) ;
void Motor_CLK_SetModeRegister(uint8 modeBitMask) ;
void Motor_CLK_ClearModeRegister(uint8 modeBitMask) ;
uint8 Motor_CLK_GetModeRegister(void) ;
void Motor_CLK_SetSourceRegister(uint8 clkSource) ;
uint8 Motor_CLK_GetSourceRegister(void) ;
#if defined(Motor_CLK__CFG3)
void Motor_CLK_SetPhaseRegister(uint8 clkPhase) ;
uint8 Motor_CLK_GetPhaseRegister(void) ;
#endif /* defined(Motor_CLK__CFG3) */

#define Motor_CLK_Enable()                       Motor_CLK_Start()
#define Motor_CLK_Disable()                      Motor_CLK_Stop()
#define Motor_CLK_SetDivider(clkDivider)         Motor_CLK_SetDividerRegister(clkDivider, 1u)
#define Motor_CLK_SetDividerValue(clkDivider)    Motor_CLK_SetDividerRegister((clkDivider) - 1u, 1u)
#define Motor_CLK_SetMode(clkMode)               Motor_CLK_SetModeRegister(clkMode)
#define Motor_CLK_SetSource(clkSource)           Motor_CLK_SetSourceRegister(clkSource)
#if defined(Motor_CLK__CFG3)
#define Motor_CLK_SetPhase(clkPhase)             Motor_CLK_SetPhaseRegister(clkPhase)
#define Motor_CLK_SetPhaseValue(clkPhase)        Motor_CLK_SetPhaseRegister((clkPhase) + 1u)
#endif /* defined(Motor_CLK__CFG3) */


/***************************************
*             Registers
***************************************/

/* Register to enable or disable the clock */
#define Motor_CLK_CLKEN              (* (reg8 *) Motor_CLK__PM_ACT_CFG)
#define Motor_CLK_CLKEN_PTR          ((reg8 *) Motor_CLK__PM_ACT_CFG)

/* Register to enable or disable the clock */
#define Motor_CLK_CLKSTBY            (* (reg8 *) Motor_CLK__PM_STBY_CFG)
#define Motor_CLK_CLKSTBY_PTR        ((reg8 *) Motor_CLK__PM_STBY_CFG)

/* Clock LSB divider configuration register. */
#define Motor_CLK_DIV_LSB            (* (reg8 *) Motor_CLK__CFG0)
#define Motor_CLK_DIV_LSB_PTR        ((reg8 *) Motor_CLK__CFG0)
#define Motor_CLK_DIV_PTR            ((reg16 *) Motor_CLK__CFG0)

/* Clock MSB divider configuration register. */
#define Motor_CLK_DIV_MSB            (* (reg8 *) Motor_CLK__CFG1)
#define Motor_CLK_DIV_MSB_PTR        ((reg8 *) Motor_CLK__CFG1)

/* Mode and source configuration register */
#define Motor_CLK_MOD_SRC            (* (reg8 *) Motor_CLK__CFG2)
#define Motor_CLK_MOD_SRC_PTR        ((reg8 *) Motor_CLK__CFG2)

#if defined(Motor_CLK__CFG3)
/* Analog clock phase configuration register */
#define Motor_CLK_PHASE              (* (reg8 *) Motor_CLK__CFG3)
#define Motor_CLK_PHASE_PTR          ((reg8 *) Motor_CLK__CFG3)
#endif /* defined(Motor_CLK__CFG3) */


/**************************************
*       Register Constants
**************************************/

/* Power manager register masks */
#define Motor_CLK_CLKEN_MASK         Motor_CLK__PM_ACT_MSK
#define Motor_CLK_CLKSTBY_MASK       Motor_CLK__PM_STBY_MSK

/* CFG2 field masks */
#define Motor_CLK_SRC_SEL_MSK        Motor_CLK__CFG2_SRC_SEL_MASK
#define Motor_CLK_MODE_MASK          (~(Motor_CLK_SRC_SEL_MSK))

#if defined(Motor_CLK__CFG3)
/* CFG3 phase mask */
#define Motor_CLK_PHASE_MASK         Motor_CLK__CFG3_PHASE_DLY_MASK
#endif /* defined(Motor_CLK__CFG3) */

#endif /* CY_CLOCK_Motor_CLK_H */


/* [] END OF FILE */
