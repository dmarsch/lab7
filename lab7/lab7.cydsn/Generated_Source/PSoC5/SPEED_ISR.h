/*******************************************************************************
* File Name: SPEED_ISR.h
* Version 1.70
*
*  Description:
*   Provides the function definitions for the Interrupt Controller.
*
*
********************************************************************************
* Copyright 2008-2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/
#if !defined(CY_ISR_SPEED_ISR_H)
#define CY_ISR_SPEED_ISR_H


#include <cytypes.h>
#include <cyfitter.h>

/* Interrupt Controller API. */
void SPEED_ISR_Start(void);
void SPEED_ISR_StartEx(cyisraddress address);
void SPEED_ISR_Stop(void);

CY_ISR_PROTO(SPEED_ISR_Interrupt);

void SPEED_ISR_SetVector(cyisraddress address);
cyisraddress SPEED_ISR_GetVector(void);

void SPEED_ISR_SetPriority(uint8 priority);
uint8 SPEED_ISR_GetPriority(void);

void SPEED_ISR_Enable(void);
uint8 SPEED_ISR_GetState(void);
void SPEED_ISR_Disable(void);

void SPEED_ISR_SetPending(void);
void SPEED_ISR_ClearPending(void);


/* Interrupt Controller Constants */

/* Address of the INTC.VECT[x] register that contains the Address of the SPEED_ISR ISR. */
#define SPEED_ISR_INTC_VECTOR            ((reg32 *) SPEED_ISR__INTC_VECT)

/* Address of the SPEED_ISR ISR priority. */
#define SPEED_ISR_INTC_PRIOR             ((reg8 *) SPEED_ISR__INTC_PRIOR_REG)

/* Priority of the SPEED_ISR interrupt. */
#define SPEED_ISR_INTC_PRIOR_NUMBER      SPEED_ISR__INTC_PRIOR_NUM

/* Address of the INTC.SET_EN[x] byte to bit enable SPEED_ISR interrupt. */
#define SPEED_ISR_INTC_SET_EN            ((reg32 *) SPEED_ISR__INTC_SET_EN_REG)

/* Address of the INTC.CLR_EN[x] register to bit clear the SPEED_ISR interrupt. */
#define SPEED_ISR_INTC_CLR_EN            ((reg32 *) SPEED_ISR__INTC_CLR_EN_REG)

/* Address of the INTC.SET_PD[x] register to set the SPEED_ISR interrupt state to pending. */
#define SPEED_ISR_INTC_SET_PD            ((reg32 *) SPEED_ISR__INTC_SET_PD_REG)

/* Address of the INTC.CLR_PD[x] register to clear the SPEED_ISR interrupt. */
#define SPEED_ISR_INTC_CLR_PD            ((reg32 *) SPEED_ISR__INTC_CLR_PD_REG)


#endif /* CY_ISR_SPEED_ISR_H */


/* [] END OF FILE */
