/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/
#include "project.h"
#include <stdio.h>

volatile int mode;
volatile uint8 period;
volatile uint8 update_period;
volatile uint8 setpoint;
volatile int speed;
volatile int animate;
uint8 TOL;
uint8 TH;
uint8 TL;
uint8 slave_address = 0x48;
char tempBuf[9];

void read_temp(uint8 slave_address, char* dataArray);
void write_deadband(uint8 slave_address, uint8 set, uint8 tol);
void access_config(uint8 slave_address, uint8 setting);

#define OFF     0
#define COOLING 1
#define HEATING 2
#define LOW     3
#define MEDIUM  4
#define HIGH    5

void temp_initialize(uint8 slave_address)
{
        uint8 Buffer1[]= {0xAC,0x02};           //sets polarity
        I2C_MasterWriteBuf((uint8)slave_address,(uint8*)Buffer1,2, I2C_MODE_COMPLETE_XFER);
        while (!(I2C_MasterStatus() & I2C_MSTAT_WR_CMPLT));
        I2C_MasterClearStatus();
        
        uint8 Buffer2[]= {0xA1,0x37,0x80};      //sets TH limit to 25.5 degrees
        I2C_MasterWriteBuf((uint8)slave_address,(uint8*)Buffer2,3, I2C_MODE_COMPLETE_XFER);
        while (!(I2C_MasterStatus() & I2C_MSTAT_WR_CMPLT));
        I2C_MasterClearStatus();
        
        uint8 Buffer3[]= {0xA2,0x36,0x80};      //sets the TL limit to 24.5 degrees
        I2C_MasterWriteBuf((uint8)slave_address,(uint8*)Buffer3,3, I2C_MODE_COMPLETE_XFER);
        while (!(I2C_MasterStatus() & I2C_MSTAT_WR_CMPLT));
        I2C_MasterClearStatus();
        
        uint8 Buffer4[]= {0xEE};                //starts convert T command
        I2C_MasterWriteBuf((uint8)slave_address,(uint8*)Buffer4,1, I2C_MODE_COMPLETE_XFER);
        while (!(I2C_MasterStatus() & I2C_MSTAT_WR_CMPLT));
        I2C_MasterClearStatus();
}

void read_temp(uint8 slave_address, char* dataArray)
{
    char disp[17];
    memset(dataArray, 0, 9);                                         //clear string before saving read data to it
    uint8 command[1];
    command[0] = 0xAA;                                              //read current temp command

    I2C_MasterWriteBuf((uint8)slave_address,command,1, I2C_MODE_COMPLETE_XFER);
    while (!(I2C_MasterStatus() & I2C_MSTAT_WR_CMPLT));
    I2C_MasterClearStatus();
    
    I2C_MasterReadBuf((uint8)slave_address,(uint8*)dataArray,2,I2C_MODE_COMPLETE_XFER);     //gets the 2 bytes of temperature data
    while (!(I2C_MasterStatus() & I2C_MSTAT_RD_CMPLT));
    I2C_MasterClearStatus();

    if (dataArray[1] == 128) dataArray[1] = 5;                                              //if the 2nd byte is 0.5C, make it a 5 to display on LCD
    sprintf(disp, "Temp: %2d.%d",dataArray[0],dataArray[1]);                                //mashing together the integers to display on LCD easily
    LCD_Position(0,0);
    LCD_PrintString(disp);
}

void write_deadband(uint8 slave_address, uint8 set, uint8 tol)
{
    uint8 command[3];
    TH = set + tol/2;
    TL = set - tol/2;
    
    command[0] = 0xA1;        
    command[1] = TH;
    command[2] = (tol % 2) == 0 ? 0x00 : 0x80;                  //check if there's a decimal which is 0.5 degrees C which is 8 bits 128 decimal value or 0x80 
    I2C_MasterWriteBuf((uint8)slave_address, command,3, I2C_MODE_COMPLETE_XFER);
    while (!(I2C_MasterStatus() & I2C_MSTAT_WR_CMPLT));
    I2C_MasterClearStatus();
 
    command[0] = 0xA2;        
    command[1] = TL;
    command[2] = (tol % 2) == 0 ? 0x00 : 0x80;
    I2C_MasterWriteBuf((uint8)slave_address, command,3, I2C_MODE_COMPLETE_XFER);
    while (!(I2C_MasterStatus() & I2C_MSTAT_WR_CMPLT));
    I2C_MasterClearStatus();
}

void access_config(uint8 slave_address, uint8 setting)
{
    uint8 command[2];
    command[0] = 0xAC;
    I2C_MasterWriteBuf((uint8)slave_address,command,1, I2C_MODE_COMPLETE_XFER);     //write the command bit first & then read
    while (!(I2C_MasterStatus() & I2C_MSTAT_WR_CMPLT));
    I2C_MasterClearStatus();
    
    command[0] = 0xAC;                                                              //access the config register by writing command
    command[1] = setting;                                                           //write setting (active high or low) to config register
    I2C_MasterWriteBuf((uint8)slave_address,command,2, I2C_MODE_COMPLETE_XFER);
    while (!(I2C_MasterStatus() & I2C_MSTAT_WR_CMPLT));
    I2C_MasterClearStatus();
}

int main(void)
{
    CyGlobalIntEnable; /* Enable global interrupts. */
    period = 199;
    update_period = 9;
    speed = LOW;
    mode = OFF;
    TOL = 1;
    setpoint = 25;
    MOTOR_ISR_Start();
    Motor_Timer_Start();
    Motor_Timer_WritePeriod(period); //initial speed for the motor to run at HIGH
    LCD_Start();
    Motor_CLK_Start();
    I2C_Start();
    TOUT_ISR_Start();    
    ENC_ISR_Start();
    TOL_ISR_Start();
    temp_initialize(slave_address);
    Update_Timer_Start();
    Update_Timer_WritePeriod(update_period);
    UPDATE_ISR_Start();
    SPEED_ISR_Start();
    MODE_ISR_Start();

    /* Place your initialization/startup code here (e.g. MyInst_Start()) */
    for(;;)
    {
        /* Place your application code here. */
    }
}

/* [] END OF FILE */
