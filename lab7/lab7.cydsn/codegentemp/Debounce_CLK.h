/*******************************************************************************
* File Name: Debounce_CLK.h
* Version 2.20
*
*  Description:
*   Provides the function and constant definitions for the clock component.
*
*  Note:
*
********************************************************************************
* Copyright 2008-2012, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_CLOCK_Debounce_CLK_H)
#define CY_CLOCK_Debounce_CLK_H

#include <cytypes.h>
#include <cyfitter.h>


/***************************************
* Conditional Compilation Parameters
***************************************/

/* Check to see if required defines such as CY_PSOC5LP are available */
/* They are defined starting with cy_boot v3.0 */
#if !defined (CY_PSOC5LP)
    #error Component cy_clock_v2_20 requires cy_boot v3.0 or later
#endif /* (CY_PSOC5LP) */


/***************************************
*        Function Prototypes
***************************************/

void Debounce_CLK_Start(void) ;
void Debounce_CLK_Stop(void) ;

#if(CY_PSOC3 || CY_PSOC5LP)
void Debounce_CLK_StopBlock(void) ;
#endif /* (CY_PSOC3 || CY_PSOC5LP) */

void Debounce_CLK_StandbyPower(uint8 state) ;
void Debounce_CLK_SetDividerRegister(uint16 clkDivider, uint8 restart) 
                                ;
uint16 Debounce_CLK_GetDividerRegister(void) ;
void Debounce_CLK_SetModeRegister(uint8 modeBitMask) ;
void Debounce_CLK_ClearModeRegister(uint8 modeBitMask) ;
uint8 Debounce_CLK_GetModeRegister(void) ;
void Debounce_CLK_SetSourceRegister(uint8 clkSource) ;
uint8 Debounce_CLK_GetSourceRegister(void) ;
#if defined(Debounce_CLK__CFG3)
void Debounce_CLK_SetPhaseRegister(uint8 clkPhase) ;
uint8 Debounce_CLK_GetPhaseRegister(void) ;
#endif /* defined(Debounce_CLK__CFG3) */

#define Debounce_CLK_Enable()                       Debounce_CLK_Start()
#define Debounce_CLK_Disable()                      Debounce_CLK_Stop()
#define Debounce_CLK_SetDivider(clkDivider)         Debounce_CLK_SetDividerRegister(clkDivider, 1u)
#define Debounce_CLK_SetDividerValue(clkDivider)    Debounce_CLK_SetDividerRegister((clkDivider) - 1u, 1u)
#define Debounce_CLK_SetMode(clkMode)               Debounce_CLK_SetModeRegister(clkMode)
#define Debounce_CLK_SetSource(clkSource)           Debounce_CLK_SetSourceRegister(clkSource)
#if defined(Debounce_CLK__CFG3)
#define Debounce_CLK_SetPhase(clkPhase)             Debounce_CLK_SetPhaseRegister(clkPhase)
#define Debounce_CLK_SetPhaseValue(clkPhase)        Debounce_CLK_SetPhaseRegister((clkPhase) + 1u)
#endif /* defined(Debounce_CLK__CFG3) */


/***************************************
*             Registers
***************************************/

/* Register to enable or disable the clock */
#define Debounce_CLK_CLKEN              (* (reg8 *) Debounce_CLK__PM_ACT_CFG)
#define Debounce_CLK_CLKEN_PTR          ((reg8 *) Debounce_CLK__PM_ACT_CFG)

/* Register to enable or disable the clock */
#define Debounce_CLK_CLKSTBY            (* (reg8 *) Debounce_CLK__PM_STBY_CFG)
#define Debounce_CLK_CLKSTBY_PTR        ((reg8 *) Debounce_CLK__PM_STBY_CFG)

/* Clock LSB divider configuration register. */
#define Debounce_CLK_DIV_LSB            (* (reg8 *) Debounce_CLK__CFG0)
#define Debounce_CLK_DIV_LSB_PTR        ((reg8 *) Debounce_CLK__CFG0)
#define Debounce_CLK_DIV_PTR            ((reg16 *) Debounce_CLK__CFG0)

/* Clock MSB divider configuration register. */
#define Debounce_CLK_DIV_MSB            (* (reg8 *) Debounce_CLK__CFG1)
#define Debounce_CLK_DIV_MSB_PTR        ((reg8 *) Debounce_CLK__CFG1)

/* Mode and source configuration register */
#define Debounce_CLK_MOD_SRC            (* (reg8 *) Debounce_CLK__CFG2)
#define Debounce_CLK_MOD_SRC_PTR        ((reg8 *) Debounce_CLK__CFG2)

#if defined(Debounce_CLK__CFG3)
/* Analog clock phase configuration register */
#define Debounce_CLK_PHASE              (* (reg8 *) Debounce_CLK__CFG3)
#define Debounce_CLK_PHASE_PTR          ((reg8 *) Debounce_CLK__CFG3)
#endif /* defined(Debounce_CLK__CFG3) */


/**************************************
*       Register Constants
**************************************/

/* Power manager register masks */
#define Debounce_CLK_CLKEN_MASK         Debounce_CLK__PM_ACT_MSK
#define Debounce_CLK_CLKSTBY_MASK       Debounce_CLK__PM_STBY_MSK

/* CFG2 field masks */
#define Debounce_CLK_SRC_SEL_MSK        Debounce_CLK__CFG2_SRC_SEL_MASK
#define Debounce_CLK_MODE_MASK          (~(Debounce_CLK_SRC_SEL_MSK))

#if defined(Debounce_CLK__CFG3)
/* CFG3 phase mask */
#define Debounce_CLK_PHASE_MASK         Debounce_CLK__CFG3_PHASE_DLY_MASK
#endif /* defined(Debounce_CLK__CFG3) */

#endif /* CY_CLOCK_Debounce_CLK_H */


/* [] END OF FILE */
